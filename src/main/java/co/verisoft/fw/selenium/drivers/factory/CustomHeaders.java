package co.verisoft.fw.selenium.drivers.factory;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * Annotation to define custom HTTP headers for tests.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface CustomHeaders {
    String value() default ""; // Headers in "Header1:Value1,Header2:Value2" format
}
