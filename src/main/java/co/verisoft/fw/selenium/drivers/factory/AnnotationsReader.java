/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package co.verisoft.fw.selenium.drivers.factory;

import co.verisoft.fw.report.observer.Report;
import co.verisoft.fw.utils.CapabilitiesReader;
import co.verisoft.fw.utils.Property;
import io.appium.java_client.MobileCommand;
import io.appium.java_client.remote.AppiumCommandExecutor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.http.HttpClient;
import org.springframework.context.ApplicationContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;


import static java.util.Arrays.asList;
import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * This class is copied from Boni Garcia code, Selenium-Jupiter. <br>
 * Original code can be found
 * <a href="https://github.com/bonigarcia/selenium-jupiter/blob/master/src/main/java/io/github/bonigarcia/seljup/AnnotationsReader.java">here</a>.
 * <br>This class reads the following annotations from the test- @DriverUrl and @DriverCapabilities <br>
 * <br><b>Exmample</b><br>
 * <pre>{@code
 * public class DriverTest {
 *     <code>@DriverUrl</code>
 *     private URL url = new URL("http://localhost:4723/");
 *
 *     <code>@DriverCapabilities</code>
 *     DesiredCapabilities capabilities = new DesiredCapabilities();
 *     {
 *         capabilities.setCapability("platformName", "android");
 *         capabilities.setCapability("deviceName", "emulator-5554");
 *         capabilities.setCapability("appPackage", "com.android.chrome");
 *         capabilities.setCapability("appActivity", "com.google.android.apps.chrome.Main");
 *         capabilities.setCapability("platformVersion", "11");
 *         capabilities.setCapability("automationName", "uiAutomator2");
 *     }
 * }
 * }</pre><br><br>
 */
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@ToString
@NoArgsConstructor
@Slf4j
public class AnnotationsReader {


    /**
     * Retrieves the capabilities either from the method parameter's annotation, a field in the test instance, or from a Spring bean.
     * The method first checks for the presence of the {@link DriverCapabilities} annotation on the method parameter.
     * If found, it attempts to retrieve the capabilities either from a JSON file or from a Spring bean.
     * If the annotation is not present, it checks if any field in the test instance is annotated with {@link DriverCapabilities}.
     *
     * @param context The Spring {@link ApplicationContext} used to retrieve the capabilities bean if necessary.
     * @param parameter The method parameter, which may be annotated with {@link DriverCapabilities}.
     * @param testInstance The test instance, which may contain fields annotated with {@link DriverCapabilities}.
     * @return An {@link Optional} containing the corresponding {@link Capabilities} if found, otherwise {@link Optional#empty()}.
     */
    public Optional<Capabilities> getCapabilities(ApplicationContext context, Parameter parameter,
                                                  Optional<Object> testInstance) {
        Optional<Capabilities> out = Optional.empty();
        DriverCapabilities driverCapabilities = parameter.getAnnotation(DriverCapabilities.class);

        // Check if the parameter contains the DriverCapabilities annotation
        if (driverCapabilities != null) {
            String driverCapabilitiesKey = driverCapabilities.value();

            // Check if there is a value-key to bean or json
            if (driverCapabilitiesKey != null) {
                // Attempt to import the capabilities from a JSON file (default path: src/test/resources/capabilities.json)
                Property property = new Property("application.properties");
                String path = property.getProperty("capability.file.path");
                String filePath = (path != null) ? path : "./src/test/resources/capabilities.json";

                // Try to get capabilities from the JSON file based on the driver capabilities key
                Capabilities capabilities = CapabilitiesReader.getDesiredCapabilities(driverCapabilitiesKey, filePath);

                // If no capabilities were found in the JSON, check for a Spring bean
                if (capabilities == null) {
                    capabilities = context.getBean(driverCapabilitiesKey, Capabilities.class);
                }

                // If capabilities were found, return them
                if (capabilities != null) {
                    out = Optional.of(capabilities);
                }
            }
        } else {
            // If no annotation on the parameter, search for DriverCapabilities in any field in the test instance
            Optional<Object> annotatedField = seekFieldAnnotatedWith(testInstance, DriverCapabilities.class);
            if (annotatedField.isPresent()) {
                // If an annotated field is found, retrieve the capabilities from it
                Capabilities capabilities = (Capabilities) annotatedField.get();
                out = Optional.of(capabilities);
            }
        }

        return out;
    }


    /**
     * Retrieves the URL either from a method parameter's annotation, a field in the test instance,
     * or from a Spring bean. The method first checks for the presence of the {@link DriverUrl} annotation
     * on the method parameter. If found, it attempts to retrieve the URL value from the annotation.
     * If not found in the parameter, it checks if any field in the test instance is annotated with {@link DriverUrl}.
     *
     * @param applicationContext The Spring {@link ApplicationContext} to retrieve the URL bean if needed.
     * @param parameter The method parameter, which may be annotated with {@link DriverUrl}.
     * @param testInstance The optional test instance, which may contain fields annotated with {@link DriverUrl}.
     * @return An {@link Optional} containing the corresponding {@link URL} if found, otherwise {@link Optional#empty()}.
     */
    public Optional<URL> getUrl(ApplicationContext applicationContext, Parameter parameter, Optional<Object> testInstance) {
        Optional<URL> out = empty();

        // Search DriverUrl annotation in parameter
        DriverUrl driverUrl = parameter.getAnnotation(DriverUrl.class);
        if (driverUrl != null) {
            Object urlValue = getUrlValueFromAnnotation(driverUrl, applicationContext);
            if (urlValue != null) {
                out = of((URL) urlValue);  // Cast urlValue to URL and wrap it in an Optional
            }
        } else {
            // If not found in parameter, search DriverUrl in any field
            Optional<Object> annotatedField = seekFieldAnnotatedWith(testInstance, DriverUrl.class);
            if (annotatedField.isPresent()) {
                out = of(convertToUrl(annotatedField.get()));  // Convert field to URL and wrap in Optional
            }
        }

        return out;
    }

    /**
     * Retrieves the URL value from the {@link DriverUrl} annotation or fetches it from a Spring bean
     * if the annotation value is not a valid URL.
     *
     * @param driverUrl The {@link DriverUrl} annotation from the method parameter.
     * @param applicationContext The Spring {@link ApplicationContext} used to fetch the URL bean if needed.
     * @return The URL object if valid, or the URL retrieved from a Spring bean if available.
     */
    private Object getUrlValueFromAnnotation(DriverUrl driverUrl, ApplicationContext applicationContext) {
        Object urlValue = driverUrl.value();
        if (isValidUrl(urlValue)) {
            return convertToUrl(urlValue);  // Convert to URL and return
        } else {
            return getUrlFromBean(driverUrl.value(), applicationContext);  // Try to get URL from Spring Bean
        }
    }

    /**
     * Validates if a given object can be converted to a valid URL.
     *
     * @param value The object to be validated.
     * @return {@code true} if the object can be converted to a valid URL, {@code false} otherwise.
     */
    private boolean isValidUrl(Object value) {
        try {
            new URL(value.toString());
            return true;
        } catch (MalformedURLException e) {
            log.warn("Invalid URL: "+ value, e);
            return false;
        }
    }

    /**
     * Converts an object to a URL.
     *
     * @param value The object to be converted to a URL.
     * @return A {@link URL} object if the conversion is successful, or {@code null} if the conversion fails.
     */
    private URL convertToUrl(Object value) {
        try {
            return new URL(value.toString());  // Convert the object to URL
        } catch (MalformedURLException e) {
            log.warn("Invalid URL: {}", value, e);
            return null;
        }
    }

    /**
     * Retrieves a URL from the Spring {@link ApplicationContext} by looking up the bean name.
     *
     * @param value The value (bean name) to look up in the Spring context.
     * @param applicationContext The Spring {@link ApplicationContext} used to retrieve the URL bean.
     * @return A {@link URL} object retrieved from the Spring context, or {@code null} if the bean cannot be found.
     */
    private URL getUrlFromBean(Object value, ApplicationContext applicationContext) {
        try {
            // Fetch the bean by name and return the URL if it exists
            return applicationContext.getBean(value.toString(), URL.class);  // Retrieve the URL bean by name
        } catch (Exception e) {
            log.warn("Error getting URL from bean: {}", value, e);
            return null;
        }
    }



    /**
     * Retrieves custom HTTP headers from various sources, including method parameter annotations,
     * test instance fields, or a Spring bean. The method prioritizes the following sources:
     * <ol>
     *     <li>The {@code @CustomHeaders} annotation on the provided method parameter.</li>
     *     <li>A field in the test instance annotated with {@code @CustomHeaders}.</li>
     *     <li>A Spring bean named {@code getCustomHeaders} from the application context.</li>
     * </ol>
     *
     * @param context      The Spring {@link ApplicationContext} used to retrieve the {@code getCustomHeaders} bean if needed.
     * @param parameter    The method parameter, which may be annotated with {@code @CustomHeaders}.
     * @param testInstance The optional test instance, which may contain fields annotated with {@code @CustomHeaders}.
     * @return An {@link Optional} containing a {@link Map} of header key-value pairs, if any are found.
     * If no headers are found in any of the sources, {@link Optional#empty()} is returned.
     */
    public Optional<Map<String, String>> getCustomHeaders(ApplicationContext context, Parameter parameter, Optional<Object> testInstance) {
        Optional<Map<String, String>> headers;
        // Check if the @CustomHeaders annotation is present on the parameter
        CustomHeaders customHeaders = parameter.getAnnotation(CustomHeaders.class);
        if (customHeaders != null) {
            headers = Optional.of(parseHeaders(customHeaders.value()));
        } else {
            // If not found on the parameter, check if there is a field with the annotation
            headers = getHeadersFromField(testInstance);
            // If still not found, try to retrieve the headers from the Spring context
            if (!headers.isPresent()) {
                headers = getHeadersFromSpringContext(context);
            }
        }
        return headers;
    }


    /**
     * Retrieves custom headers from a field in the test instance that is annotated with {@code @CustomHeaders}.
     *
     * @param testInstance The optional test instance that may contain fields annotated with {@code @CustomHeaders}.
     * @return An {@link Optional} containing a {@link Map} of header key-value pairs, if any are found.
     * If no annotated fields are found, {@link Optional#empty()} is returned.
     */
    private Optional<Map<String, String>> getHeadersFromField(Optional<Object> testInstance) {
        if (!testInstance.isPresent()) {
            Report.info("Test instance is not present. Cannot search for fields annotated with @CustomHeaders.");
            return Optional.empty();
        }

        Optional<Field> annotatedField = seekFieldAnnotatedWithField(testInstance, CustomHeaders.class);
        if (annotatedField.isPresent()) {
            return extractHeadersFromField(annotatedField.get(), testInstance.get());
        }
        return Optional.empty();
    }

    /**
     * Retrieves custom headers from the Spring context if available.
     *
     * @param context The Spring {@link ApplicationContext} used to retrieve the headers.
     * @return An {@link Optional} containing a {@link Map} of header key-value pairs, if any are found.
     * If no headers are found, {@link Optional#empty()} is returned.
     */
    private Optional<Map<String, String>> getHeadersFromSpringContext(ApplicationContext context) {
        try {
            Map<String, String> beanHeaders = context.getBean("getCustomHeaders", Map.class);
            if (beanHeaders != null && !beanHeaders.isEmpty()) {
                return Optional.of(beanHeaders);
            }
        } catch (Exception e) {
            Report.info("Error retrieving custom headers from Spring context", e);
        }
        return Optional.empty();
    }


    /**
     * Extracts the headers from a field annotated with {@code @CustomHeaders}.
     *
     * @param field        The field annotated with {@code @CustomHeaders}.
     * @param testInstance The test instance that holds the field.
     * @return An {@link Optional} containing a {@link Map} of header key-value pairs, if the field contains a valid Map.
     * If the field does not contain a Map or cannot be accessed, {@link Optional#empty()} is returned.
     */
    private Optional<Map<String, String>> extractHeadersFromField(Field field, Object testInstance) {
        try {
            field.setAccessible(true);
            Object fieldValue = field.get(testInstance);
            if (fieldValue instanceof Map) {
                return Optional.of((Map<String, String>) fieldValue);
            }
        } catch (IllegalAccessException e) {
            Report.info("Error accessing field value for custom headers", e);
        }
        return Optional.empty();
    }

    /**
     * Searches for a field annotated with the specified annotation class in the provided test instance.
     *
     * @param testInstance  The test instance containing the fields to search.
     * @param annotationClass The annotation class to search for on fields.
     * @return An {@link Optional} containing the first field found with the specified annotation, if any.
     * If no annotated fields are found, {@link Optional#empty()} is returned.
     */
    private Optional<Field> seekFieldAnnotatedWithField(Optional<Object> testInstance, Class<? extends Annotation> annotationClass) {
        if (!testInstance.isPresent()) {
            Report.info("Test instance is not present. Cannot search for fields annotated with {}", annotationClass.getSimpleName());
            return Optional.empty();
        }
        Object instance = testInstance.get();
        for (Field field : instance.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(annotationClass)) {
                return Optional.of(field);
            }
        }
        return Optional.empty();
    }

    /**
     * Parses a string of headers into a {@link Map}.
     *
     * @param headersString A string of headers in the format "Header1:Value1,Header2:Value2".
     * @return A {@link Map} of header key-value pairs.
     */
    private Map<String, String> parseHeaders(String headersString) {
        return Arrays.stream(headersString.split(","))
                .map(entry -> entry.split(":"))
                .collect(Collectors.toMap(entry -> entry[0].trim(), entry -> entry[1].trim()));
    }

    /**
     * Retrieves the command executor based on the given parameter and context.
     * Looks for a {@link DriverCommandExecutor} annotation on the parameter or in a field in the test instance.
     * If found, it either creates or retrieves the corresponding executor.
     *
     * @param applicationContext the Spring context to retrieve beans
     * @param parameter the parameter annotated with {@link DriverCommandExecutor}
     * @param testInstance the test instance containing potential annotated fields
     * @return an {@link Optional} with the executor or empty if not found or an error occurs
     */
    public Optional<Object> getCommandExecutor(ApplicationContext applicationContext, Parameter parameter,
                                               Optional<Object> testInstance) {
        Optional<Object> out = empty();

        try {
            Object commandExecutorValue;
            DriverCommandExecutor commandExecutor = parameter.getAnnotation(DriverCommandExecutor.class);
            if (commandExecutor != null) {
                try {
                    // create a default AppiumCommandExecutor by class name
                    commandExecutorValue = commandExecutor.value();
                    Class<?> cls = Class.forName((String) commandExecutorValue);
                    HttpClient.Factory instance = (HttpClient.Factory) cls.getDeclaredConstructor().newInstance();
                    Optional<URL> optionalUrl = getUrl(applicationContext, parameter, testInstance);
                    URL url = optionalUrl.orElse(new URL("http://127.0.0.1:4723/wd/hub/"));
                    out = Optional.of(new AppiumCommandExecutor(MobileCommand.commandRepository, url, instance));
                    return out;
                } catch (ClassNotFoundException e) {
                    //if there is no such class, try to get a bean
                    commandExecutorValue = applicationContext.getBean(commandExecutor.value(), HttpCommandExecutor.class);
                    out = Optional.of(commandExecutorValue);
                }
            } else {
                // If not, search DriverCommandExecutor in any field
                Optional<Object> annotatedField = seekFieldAnnotatedWith(
                        testInstance, DriverCommandExecutor.class);
                if (annotatedField.isPresent()) {
                    commandExecutorValue = annotatedField.get();
                    out = Optional.of(commandExecutorValue);
                }
            }

        } catch (Exception e) {
            log.warn("Exception getting HttpCommandExecutor", e);
        }
        return out;
    }



    public boolean isBoolean(String s) {
        boolean isBool = s.equalsIgnoreCase("true")
                || s.equalsIgnoreCase("false");
        if (!isBool) {
            log.trace("Value {} is not boolean", s);
        }
        return isBool;
    }


    public boolean isNumeric(String s) {
        boolean numeric = StringUtils.isNumeric(s);
        if (!numeric) {
            log.trace("Value {} is not numeric", s);
        }
        return numeric;
    }


    public <T> T getFromAnnotatedField(Optional<Object> testInstance,
                                       Class<? extends Annotation> annotationClass,
                                       Class<T> capabilitiesClass) {
        if (capabilitiesClass == null) {
            throw new RuntimeException("The parameter capabilitiesClass must not be null");
        }
        return seekFieldAnnotatedWith(testInstance, annotationClass,
                capabilitiesClass).orElse(null);
    }


    public Optional<Object> seekFieldAnnotatedWith(
            Optional<Object> testInstance,
            Class<? extends Annotation> annotation) {
        return seekFieldAnnotatedWith(testInstance, annotation, null);
    }


    private static <T> Optional<T> seekFieldAnnotatedWith(
            Optional<Object> testInstance,
            Class<? extends Annotation> annotation, Class<T> annotatedType) {
        Optional<T> out = empty();
        try {
            if (testInstance.isPresent()) {
                Object object = testInstance.get();
                Class<?> clazz = object.getClass();
                out = getField(annotation, annotatedType, clazz, object);
                if (!out.isPresent()) {
                    // If annotation not present in class, look for it in the
                    // parent(s)
                    Class<?> superclass;
                    while ((superclass = clazz
                            .getSuperclass()) != Object.class) {
                        out = getField(annotation, annotatedType, superclass,
                                object);
                        if (out.isPresent()) {
                            break;
                        }
                        clazz = clazz.getSuperclass();
                    }
                }
            }
        } catch (Exception e) {
            log.warn("Exception seeking field in {} annotated with {}",
                    annotatedType, annotation, e);
        }
        return out;
    }


    @SuppressWarnings("unchecked")
    private static <T> Optional<T> getField(
            Class<? extends Annotation> annotation, Class<T> annotatedType,
            Class<?> clazz, Object object)
            throws IllegalAccessException {
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(annotation) && (annotatedType == null
                    || annotatedType.isAssignableFrom(field.getType()))) {
                field.setAccessible(true);
                if (annotatedType != null) {
                    return of(annotatedType.cast(field.get(object)));
                }
                return (Optional<T>) of(field.get(object));
            }
        }
        return empty();
    }


    public Optional<List<Object>> getKeyValue(String keyValue) {
        StringTokenizer st = new StringTokenizer(keyValue, "=");
        if (st.countTokens() != 2) {
            log.warn("Invalid format in {} (expected key=value)", keyValue);
            return empty();
        }
        String key = st.nextToken();
        String value = st.nextToken();
        Object returnedValue = value;
        if (isBoolean(value)) {
            returnedValue = Boolean.valueOf(value);
        } else if (isNumeric(value)) {
            returnedValue = Integer.valueOf(value);
        }
        return of(asList(key, returnedValue));
    }
}
