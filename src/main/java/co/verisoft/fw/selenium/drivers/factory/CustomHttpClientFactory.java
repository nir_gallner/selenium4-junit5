package co.verisoft.fw.selenium.drivers.factory;
import org.openqa.selenium.remote.http.HttpClient;
import org.openqa.selenium.remote.http.HttpRequest;
import org.openqa.selenium.remote.http.HttpResponse;
import org.openqa.selenium.remote.http.WebSocket;
import java.util.Map;
public class CustomHttpClientFactory {
    /**
     * Creates a custom HttpClient.Factory that adds headers to every HTTP request.
     *
     * @param headers Headers to include in the HTTP requests
     * @return A customized HttpClient.Factory
     */
    public static HttpClient.Factory createCustomHttpClientFactory(Map<String, String> headers) {
        return config -> new HttpClient() {
            private final HttpClient delegate = HttpClient.Factory.createDefault().createClient(config);
            @Override
            public HttpResponse execute(HttpRequest request) {
                if (headers != null && !headers.isEmpty()) {
                    headers.forEach(request::addHeader);
                }
                return delegate.execute(request);
            }
            @Override
            public WebSocket openSocket(HttpRequest request, WebSocket.Listener listener) {
                return delegate.openSocket(request, listener);
            }
            @Override
            public void close() {
                delegate.close();
            }
        };
    }
}
