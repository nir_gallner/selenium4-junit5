//package co.verisoft.fw.selenium.drivers.factory;
package co.verisoft.fw.selenium.drivers.factory;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.http.HttpClient;

import java.net.URL;
import java.util.HashMap;

/**
 * RemoteWebDriverExtension is a customized extension of the {@link org.openqa.selenium.remote.RemoteWebDriver} class.
 * This class is designed to provide additional flexibility and customization by allowing the injection of a custom
 * {@link org.openqa.selenium.remote.http.HttpClient.Factory} for HTTP command execution.
 * <p>
 * It enables users to specify a remote Selenium server URL, custom HTTP client settings, and desired capabilities
 * for the remote WebDriver session. This can be particularly useful in scenarios where specific HTTP client
 * configurations or headers are required.
 * </p>
 *
 * @see org.openqa.selenium.remote.RemoteWebDriver
 * @see org.openqa.selenium.remote.http.HttpClient.Factory
 * @see org.openqa.selenium.Capabilities
 *
 * @author <a href="mailto:esther.nachum@verisoft.co">Esther Nahum</a>
 * @since 01.25
 * */
public class RemoteWebDriverExtension extends RemoteWebDriver {

    public RemoteWebDriverExtension(URL remoteAddress, HttpClient.Factory customFactory, Capabilities capabilities) {
        super(new HttpCommandExecutor(new HashMap<>(),remoteAddress,customFactory),capabilities);
    }
}
